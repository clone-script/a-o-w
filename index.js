/* eslint-disable node/no-deprecated-api */
const cheerio = require('cheerio')
const loadJsonFile = require('load-json-file')
const axios = require('axios')
const URL = require('url')

const fetch = async (url) => {
  try {
    const { data } = await axios.get(url)
    return data
  } catch (error) {
    await new Promise(resolve => setTimeout(resolve, 10000))
    return false
  }
}

const URLs = async () => {
  let urls = []
  for (let i = 0; i < 2; i++) {
    let html = await fetch('https://www.aowvn.org/feeds/posts/default?max-results=500&alt=json-in-script&callback=loadtoc&start-index=' + (150 * i + 1))
    html = html.replace('// API callback', '')
    html = html.replace('loadtoc(', '')
    html = html.replace(/\);$/, '')
    const json = JSON.parse(html)
    urls = [...urls, ...json.feed.entry.map((en) => en.link[2].href)]
  }
  return urls
}

const read = require('read-file')
const write = require('write-file-utf8')
const _ = require('lodash')
const writeJsonFile = require('write-json-file')

const save = async () => {
  const urls = await loadJsonFile('docs/index.html')
  for (let i = 0; i < urls.length; i++) {
    console.log('🚀 ~ save ~ i', i)
    const url = urls[i]
    const qURL = URL.parse(url, true)
    const pathURL = 'docs' + qURL.pathname + '.html'
    try {
      read.sync(pathURL)
    } catch (error) {
      const html = await fetch(url)
      if (!html) {
        continue
      }
      await write(pathURL, html)
    }
  }
}

URLs().then((urls) => {
  writeJsonFile('docs/index.html', urls).then(() => {
    save()
  })
})

module.exports = {
  URLs,
  save
}
